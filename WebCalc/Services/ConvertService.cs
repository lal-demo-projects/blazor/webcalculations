﻿using System;
using System.Globalization;
using WebCalc.Constants;

namespace WebCalc.Services
{
    public class ConvertService
    {
        public double ToDouble(object value)
        {
            double result = 0;
            if (value != null)
                _ = double.TryParse(value.ToString(), out result);
            return result;
        }

        public int ToInt32(object value)
        {
            int result = 0;
            if (value != null)
                _ = int.TryParse(value.ToString(), out result);
            return result;
        }

        public string ToIndianCurrency(double value, bool includePrefix, bool includeSuffix)
        {
            NumberFormatInfo myNumberFormatInfo = new CultureInfo("hi-IN", false).NumberFormat;
            myNumberFormatInfo.CurrencySymbol = includePrefix ? "Rs. " : string.Empty;
            myNumberFormatInfo.CurrencyGroupSizes = new int[] { 3, 2 };
            return value.ToString("C2", myNumberFormatInfo) + (includeSuffix ? "/-" : string.Empty);
        }

        public double ConvertLength(LengthType from, LengthType to, double value)
        {
            double heightInCentimeter = from switch
            {
                LengthType.Meter => value * 100,
                LengthType.Feet => value / (3.2808f / 100),
                LengthType.Centimeter => value,
                _ => throw new NotImplementedException(),
            };
            double lengthResult = to switch
            {
                LengthType.Meter => heightInCentimeter / 100,
                LengthType.Feet => heightInCentimeter * (3.2808f / 100),
                LengthType.Centimeter => heightInCentimeter,
                _ => throw new NotImplementedException(),
            };
            return lengthResult;
        }

        public string GetLengthAbbreviation(LengthType type)
        {
            string abbr = type switch
            {
                LengthType.Meter => "m",
                LengthType.Feet => "f",
                LengthType.Centimeter => "cm",
                _ => throw new NotImplementedException(),
            };
            return abbr;
        }

        public double ConvertMass(MassType from, MassType to, double value)
        {
            double weightInGram = from switch
            {
                MassType.Gram => value,
                MassType.Kilogram => value * 1000,
                MassType.Pound => value / (2.2046f * 1000),
                MassType.Stone => value / (0.15747f * 1000),
                _ => throw new NotImplementedException(),
            };
            double weightResult = to switch
            {
                MassType.Gram => weightInGram,
                MassType.Kilogram => weightInGram / 1000,
                MassType.Pound => weightInGram * (2.2046f / 1000),
                MassType.Stone => weightInGram * (0.15747f / 1000),
                _ => throw new NotImplementedException(),
            };
            return weightResult;
        }

        public string GetMassAbbreviation(MassType type)
        {
            string abbr = type switch
            {
                MassType.Gram => "g",
                MassType.Kilogram => "kg",
                MassType.Pound => "lb",
                MassType.Stone => "st",
                _ => throw new NotImplementedException(),
            };
            return abbr;
        }

        public double ConvertTemperature(TemperatureType from, TemperatureType to, double value)
        {
            double tempInKelvin = from switch
            {
                TemperatureType.Fahrenheit => ((value - 32) * ((double)5 / 9)) + 273.15,
                TemperatureType.Celsius => value + 273.15,
                TemperatureType.Kelvin => value,
                _ => throw new NotImplementedException(),
            };
            double tempResult = to switch
            {
                TemperatureType.Fahrenheit => ((tempInKelvin - 273.15) * ((double)9 / 5)) + 32,
                TemperatureType.Celsius => tempInKelvin - 273.15,
                TemperatureType.Kelvin => tempInKelvin,
                _ => throw new NotImplementedException(),
            };
            return tempResult;
        }

        public string GetTemperatureAbbreviation(TemperatureType type)
        {
            string abbr = type switch
            {
                TemperatureType.Fahrenheit => "F",
                TemperatureType.Celsius => "C",
                TemperatureType.Kelvin => "K",
                _ => throw new NotImplementedException(),
            };
            return abbr;
        }
    }
}
