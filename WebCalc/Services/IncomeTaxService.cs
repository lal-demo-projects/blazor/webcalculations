﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace WebCalc.Services
{
    public enum TaxPayerType { Individual, HinduUndividedFamily }
    public enum TaxScheme { OldScheme, NewScheme_115BAC_2223, NewScheme_115BAC_2324 }

    public class ITDataInput
    {
        public TaxPayerType TaxPayerType { get; set; } = TaxPayerType.Individual;
        public int Age { get; set; } = 50;

        public TaxScheme TaxScheme { get; set; } = TaxScheme.OldScheme;

        public double IncomeFromSalary { get; set; } = 0;

        public double RentPaid { get; set; } = 0;
        public double HRA { get; set; } = 0;
        public bool IsLivingInMetroCities { get; set; } = false;

        public double Section80C { get; set; } = 0;
        public double Section80D { get; set; } = 0;
        public double Section80TTA { get; set; } = 0;
        public double Section24B { get; set; } = 0;
    }

    public class ITDataOutput
    {
        public double IncomeFromSalary { get; set; } = 0;
        public double NetIncome { get; set; } = 0;
        public Dictionary<string, double> Deductions { get; set; } = new Dictionary<string, double>();
        public double TotalDeductions { get; set; } = 0;
        public double TaxableIncome { get; set; } = 0;
        public Dictionary<string, double> TaxAmountSplits { get; set; } = new Dictionary<string, double>();
        public double NetTaxAmount { get; set; } = 0;
        public double SurchargePercentage { get; set; } = 0;
        public double SurchargeAmount { get; set; } = 0;
        public double Rebate87A { get; set; } = 0;
        public double CessPercentage { get; set; } = 0;
        public double CessAmount { get; set; } = 0;
        public double TaxAmount { get; set; } = 0;
    }

    public class TaxSlab
    {
        public List<TaxSlabItem> TaxSlabItems { get; set; }
        public double StandardDeduction { get; set; } = 0;
        public double MaxSection80C { get; set; } = 0;
        public double MaxSection80D { get; set; } = 0;
        public double MaxSection80TTA { get; set; } = 0;
        public double MaxSection24B { get; set; } = 0;
        public double MaxRebate87A { get; set; } = 0;
        public double CessPercentage { get; set; } = 0;
    }

    public class TaxSlabItem
    {
        public double MinAmount { get; set; }
        public double FromAmount { get; set; }
        public double? ToAmount { get; set; }
        public double Percentage { get; set; }
    }

    internal static class TaxSlabFactory
    {
        public static TaxSlab GetTaxSlab(ITDataInput inputData)
        {
            TaxSlab taxSlab = new TaxSlab()
            {
                TaxSlabItems = null,
                MaxRebate87A = inputData.TaxPayerType == TaxPayerType.Individual ? 12_500 : 0,
                CessPercentage = 4
            };
            if (inputData.TaxScheme == TaxScheme.OldScheme)
            {
                taxSlab.StandardDeduction = 50_000;
                taxSlab.MaxSection80C = 1_50_000;
                taxSlab.MaxSection80D = 25_000;
                taxSlab.MaxSection80TTA = 10_000;
                taxSlab.MaxSection24B = 12_500;
            }
            else
            {
                taxSlab.StandardDeduction = 0;
                taxSlab.MaxSection80C = 0;
                taxSlab.MaxSection80D = 0;
                taxSlab.MaxSection80TTA = 0;
                taxSlab.MaxSection24B = 0;
            }

            if (inputData.TaxScheme == TaxScheme.NewScheme_115BAC_2223)
            {
                taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                {
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 5_00_000, Percentage = 0 },
                    new TaxSlabItem() { MinAmount = 5_00_000, FromAmount = 2_50_000, ToAmount = 5_00_000, Percentage = 5 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 5_00_000, ToAmount = 7_50_000, Percentage = 10 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 7_50_000, ToAmount = 10_00_000, Percentage = 15 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 10_00_000, ToAmount = 12_50_000, Percentage = 20 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 12_50_000, ToAmount = 15_00_000, Percentage = 25 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 15_00_000, ToAmount = null, Percentage = 30 }
                };
            }
            else if (inputData.TaxScheme == TaxScheme.NewScheme_115BAC_2324)
            {
                taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                {
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 3_00_000, Percentage = 0 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 3_00_000, ToAmount = 6_00_000, Percentage = 5 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 6_00_000, ToAmount = 9_00_000, Percentage = 10 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 9_00_000, ToAmount = 12_00_000, Percentage = 15 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 12_00_000, ToAmount = 15_00_000, Percentage = 20 },
                    new TaxSlabItem() { MinAmount = 0, FromAmount = 15_00_000, ToAmount = null, Percentage = 30 }
                };
            }
            else if (inputData.TaxPayerType == TaxPayerType.HinduUndividedFamily)
            {
                taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                    {
                        new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 2_50_000, Percentage = 0 },
                        new TaxSlabItem() { MinAmount = 0, FromAmount = 2_50_000, ToAmount = 5_00_000, Percentage = 5 },
                        new TaxSlabItem() { MinAmount = 0, FromAmount = 5_00_000, ToAmount = 10_00_000, Percentage = 20 },
                        new TaxSlabItem() { MinAmount = 0, FromAmount = 10_00_000, ToAmount = null, Percentage = 30 },
                    };
            }
            else if (inputData.TaxPayerType == TaxPayerType.Individual)
            {
                if (inputData.Age < 60)
                {
                    taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                        {
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 2_50_000, Percentage = 0 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 2_50_000, ToAmount = 5_00_000, Percentage = 5 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 5_00_000, ToAmount = 10_00_000, Percentage = 20 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 10_00_000, ToAmount = null, Percentage = 30 },
                        };
                }
                else if (inputData.Age >= 60 && inputData.Age < 80)
                {
                    taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                        {
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 3_00_000, Percentage = 0 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 3_00_000, ToAmount = 5_00_000, Percentage = 5 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 5_00_000, ToAmount = 10_00_000, Percentage = 20 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 10_00_000, ToAmount = null, Percentage = 30 },
                        };
                }
                else if (inputData.Age >= 80)
                {
                    taxSlab.TaxSlabItems = new List<TaxSlabItem>()
                        {
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 0, ToAmount = 5_00_000, Percentage = 0 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 5_00_000, ToAmount = 10_00_000, Percentage = 20 },
                            new TaxSlabItem() { MinAmount = 0, FromAmount = 10_00_000, ToAmount = null, Percentage = 30 },
                        };
                }
                else throw new NotImplementedException();
            }
            else throw new NotImplementedException();

            return taxSlab;
        }
    }

    public class IncomeTaxService
    {
        private double GetLowestAmount(double input, double max)
        {
            double result = 0;
            if (input > 0)
            {
                result = input > max ? max : input;
            }
            return result;
        }
        private ITDataInput GetValidInputData(ITDataInput inputData)
        {
            return new ITDataInput()
            {
                TaxPayerType = inputData.TaxPayerType,
                Age = inputData.Age < 0 ? 0 : inputData.Age,

                TaxScheme = inputData.TaxScheme,

                IncomeFromSalary = inputData.IncomeFromSalary < 0 ? 0 : inputData.IncomeFromSalary,

                RentPaid = inputData.RentPaid < 0 ? 0 : inputData.RentPaid,
                HRA = inputData.HRA < 0 ? 0 : inputData.HRA,
                IsLivingInMetroCities = inputData.IsLivingInMetroCities,

                Section80C = inputData.TaxScheme != TaxScheme.OldScheme || inputData.Section80C < 0 ? 0 : inputData.Section80C,
                Section80D = inputData.TaxScheme != TaxScheme.OldScheme || inputData.Section80D < 0 ? 0 : inputData.Section80D,
                Section80TTA = inputData.TaxScheme != TaxScheme.OldScheme || inputData.Section80TTA < 0 ? 0 : inputData.Section80TTA,
                Section24B = inputData.TaxScheme != TaxScheme.OldScheme || inputData.Section24B < 0 ? 0 : inputData.Section24B
            };
        }
        private void CalculateSurcharge(ITDataOutput dataOutput)
        {
            // Surcharge: 10% of income tax, where total income exceeds Rs.50 lakh up to Rs.1 crore. 15% of income tax, where the total income exceeds Rs.1 crore.
            dataOutput.SurchargePercentage = dataOutput.NetIncome switch
            {
                < 50_00_000 => 0,
                >= 50_00_000 and < 1_00_00_000 => 10,
                >= 1_00_00_000 and < 2_00_00_000 => 15,
                _ => 25,
            };
            dataOutput.SurchargeAmount = Math.Round(dataOutput.NetTaxAmount * (dataOutput.SurchargePercentage / 100), 2);
        }
        public ITDataOutput CalculateTax(ITDataInput inputData)
        {
            inputData = GetValidInputData(inputData);

            TaxSlab taxSlab = TaxSlabFactory.GetTaxSlab(inputData);
            ITDataOutput result = new() { NetIncome = inputData.IncomeFromSalary };

            if (inputData.TaxScheme == TaxScheme.OldScheme)
            {
                result.Deductions.Add("Standard Deduction", taxSlab.StandardDeduction);
                if (inputData.HRA > 0)
                {
                    double percentOfSalary = inputData.IncomeFromSalary * (inputData.IsLivingInMetroCities ? 50 : 40) / 100;
                    double percent10OfSalary = inputData.RentPaid - (inputData.IncomeFromSalary * 10 / 100);
                    double HRAExemption = GetLowestAmount(percentOfSalary, percent10OfSalary);
                    HRAExemption = GetLowestAmount(HRAExemption, inputData.HRA);
                    if (HRAExemption < 0) HRAExemption = 0;
                    result.Deductions.Add("HRA Exemption", HRAExemption);
                }
                result.Deductions.Add("Section 80C", GetLowestAmount(inputData.Section80C, taxSlab.MaxSection80C));
                result.Deductions.Add("Section 80D", GetLowestAmount(inputData.Section80D, taxSlab.MaxSection80D));
                result.Deductions.Add("Section 80TTA", GetLowestAmount(inputData.Section80TTA, taxSlab.MaxSection80TTA));
            }

            result.TotalDeductions = result.Deductions.Values.Sum();
            result.TaxableIncome = result.NetIncome - result.TotalDeductions;
            if (result.TaxableIncome <= 0)
                result.TaxableIncome = 0;
            else
            {
                double itemAmount;
                foreach (TaxSlabItem item in taxSlab.TaxSlabItems)
                {
                    if (result.TaxableIncome > item.MinAmount && result.TaxableIncome > item.FromAmount)
                    {
                        itemAmount = GetLowestAmount(result.TaxableIncome, item.ToAmount ?? result.TaxableIncome);
                        itemAmount = (itemAmount - item.FromAmount) * (item.Percentage / 100);
                        result.TaxAmountSplits.Add($"Rs. {item.FromAmount} to Rs. {item.ToAmount} @ {item.Percentage}%", itemAmount);
                    }
                    else result.TaxAmountSplits.Add($"Rs. {item.FromAmount} to Rs. {item.ToAmount} @ {item.Percentage}%", 0);
                }
                result.NetTaxAmount = result.TaxAmountSplits.Values.Sum();

                CalculateSurcharge(result);

                // Rebate under Section 87A can be claimed by any resident Indian whose total annual income is below Rs.5 lakh. The maximum available rebate under 87A is Rs.12,500.
                if (result.NetIncome < 5_00_000)
                    result.Rebate87A = GetLowestAmount(result.NetTaxAmount, taxSlab.MaxRebate87A);

                result.TaxAmount = result.NetTaxAmount + result.SurchargeAmount - result.Rebate87A;

                if (result.TaxAmount > 0)
                {
                    result.CessPercentage = taxSlab.CessPercentage;
                    result.CessAmount = Math.Round(result.NetTaxAmount * (result.CessPercentage / 100), 2);
                    result.TaxAmount += result.CessAmount;
                }
            }
            return result;
        }
    }
}
