﻿
namespace WebCalc.Constants
{
    public enum TemperatureType { Fahrenheit, Celsius, Kelvin }
}
